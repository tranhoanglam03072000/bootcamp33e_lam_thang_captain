let LINKURL = "https://62f8b754e0564480352bf3de.mockapi.io";
let dssp = [];
let arrItemCart = [];
//saveLocalStorage
let saveLocalStorage = () => {
  let arrItemCartJson = JSON.stringify(arrItemCart);
  localStorage.setItem("CART", arrItemCartJson);
};
//Lấy LocalStorage
let arrItemCartLocalStorage = localStorage.getItem("CART");
if (JSON.parse(arrItemCartLocalStorage)) {
  arrItemCart = JSON.parse(arrItemCartLocalStorage);
  inSanPhamRaGioHang(arrItemCart);
  demSoLuong(arrItemCart);
  tongTien(arrItemCart);
}
//In tất cả sản phẩm
let inSanPham = () => {
  turnOnLoading();
  axios({
    url: `${LINKURL}/phone`,
    method: "GET",
  })
    .then((res) => {
      dssp = res.data;
      inSanPhamRaManHinh(res.data);
      inLoaiSanPham();
      turnOffLoading();
    })
    .catch((err) => {
      turnOffLoading();
      alert("Lỗi mất rồi");
    });
};
inSanPham();
// add sản phẩm ở dssp
let addProduct = (id) => {
  let index = indexProduct(dssp, id);
  let spCart = new cartList_(dssp[index]);
  //Phần 2
  arrItemCart.push(spCart);
  inputCart(id);
  inSanPhamRaGioHang(arrItemCart);
  demSoLuong(arrItemCart);
  tongTien(arrItemCart);
  saveLocalStorage();
};
// tăng và giảm sản phẩm ở ô input
let addSubtractInput = (id) => {
  let index = indexProDuctCart(arrItemCart, id);
  let arrIndex = arrItemCart[index];
  arrIndex.amount = domInputAddSubtract(id) * 1;
  removeIconSp(arrItemCart, index, id);
  inSanPhamRaGioHang(arrItemCart);
  demSoLuong(arrItemCart);
  tongTien(arrItemCart);
  saveLocalStorage();
};
// add số lượng sản phẩm ở trong giot hàng
let addProductCart = (id) => {
  let index = indexProDuctCart(arrItemCart, id);
  arrItemCart[index].amount += 1;
  let arrIndex = arrItemCart[index];
  inputCart(id);
  domSelec(`#inputAddSubtract${id}`).value = arrIndex.amount;
  demSoLuong(arrItemCart);
  domSelec(`#cartItem${id}`).innerText = arrItemCart[index].amount;
  tongTien(arrItemCart);
  saveLocalStorage();
};
// subtact số lượng sản phẩm trong giot hàng
let subtractProductCart = (id) => {
  let index = indexProDuctCart(arrItemCart, id);
  let arrIndex = arrItemCart[index];
  arrIndex.amount -= 1;
  inputCart(id);
  domSelec(`#inputAddSubtract${id}`).value = arrIndex.amount;
  removeIconSp(arrItemCart, index, id);
  domSelec(`#cartItem${id}`).innerText = arrIndex.amount;
  inSanPhamRaGioHang(arrItemCart);
  tongTien(arrItemCart);
  demSoLuong(arrItemCart);
  saveLocalStorage();
};
// clear từng loại trong giỏ hàng
let clearSpCart = (id) => {
  let indexSanPhamTrongGioHang = indexProDuctCart(arrItemCart, id);
  arrItemCart.splice(indexSanPhamTrongGioHang, 1);
  inSanPhamRaGioHang(arrItemCart);
  tongTien(arrItemCart);
  demSoLuong(arrItemCart);
  if (domSelec(`#ListInputAddSubtract${id}`)) {
    domIconAddCart(id);
  }
  saveLocalStorage();
};
// Xác nhận thanh toán
let xacNhanThanhToanVaClearAll = () => {
  arrItemCart = [];
  inSanPhamRaGioHang(arrItemCart);
  domSelec("#loaiSanPham").value = 0;
  inSanPhamRaManHinh(dssp);
  tongTien(arrItemCart);
  demSoLuong(arrItemCart);
  saveLocalStorage();
};
//In loại sản phẩm
let inLoaiSanPham = () => {
  let danhSach = loaiSanPham(dssp);
  inSanPhamRaManHinh(danhSach);
  arrItemCart.forEach((item) => {
    danhSach.forEach((sanpham) => {
      if (sanpham.id == item.product.id) {
        let id = item.product.id;
        if (item.amount > 0) {
          inputCart(id);
          domSelec(`#inputAddSubtract${id}`).value = item.amount;
        } else {
          domIconAddCart(id);
        }
      }
    });
  });
};
//
