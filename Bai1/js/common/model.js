let domSelec = (id) => {
  return document.querySelector(id);
};
let domInputAddSubtract = (id) => {
  return domSelec(`#inputAddSubtract${id}`).value;
};
let domIconAddCart = (id) => {
  domSelec(
    `#ListInputAddSubtract${id}`
  ).innerHTML = `<a  onclick="addProduct('${id}')" href="#" class="btn btn-primary"><i class="fa fa-cart-plus" ></i></a>`;
};
// thẻ một sản phẩm
let sanPham = (sp) => {
  return `<div class="card item_sanpham mb-5" style="width: 18rem;">
    <div  class="card-top"><img src=${sp.img} alt="..."></div>
    <div class="card-body">
      <h5 class="card-title">${sp.name} <br> <span>(${sp.desc})</span> </h5>
      <p class="card-text">
       <span><i class="fa fa-mobile-alt"></i> : ${sp.screen}</span> <br>
      <span><i class="fa fa-camera"></i> : trước ${sp.frontCamera}, sau : ${
    sp.backCamera
  }
      </span>
      </p>
      <div class="row footer_card">
      <span class="text-danger h3 col-6">${(
        sp.price * 1
      ).toLocaleString()} $</span>
      <span class="offset-2 col-3" id="ListInputAddSubtract${sp.id}">
      <a  onclick="addProduct('${
        sp.id
      }')"  class="btn btn-primary"><i class="fa fa-cart-plus" ></i></a>
      </span>
      </div>
    </div>
    </div>`;
};
// lớp sản phẩm trong giỏ hàng
class cartList_ {
  product;
  amount;
  constructor(proDuct) {
    this.product = proDuct;
    this.amount = 1;
  }
}
// in các sản phẩm trong giỏ hàng
let ListcartItem = (listsp) => {
  let listCart = ``;
  listsp.forEach((sp) => {
    let cartIteam = `<div class="cart_itemproduct row">
    <div class="cart_img offset-1 col-2">
      <img
        src="${sp.product.img}"
        alt=""
      />
    </div>
    <div class="cart_namephone col-2">${sp.product.name}</div>
    <div class="cart_amount col-2">
      <i class="fa fa-caret-left" onclick="subtractProductCart('${
        sp.product.id
      }')"></i>
      <span id="cartItem${sp.product.id}">${sp.amount}</span>
      <i class="fa fa-caret-right" onclick="addProductCart('${
        sp.product.id
      }')"></i>
    </div>
    <div class="cart_price col-2">${(
      sp.product.price * sp.amount
    ).toLocaleString()} $</div>
    <div class="cart_trash col-2" onclick="clearSpCart('${
      sp.product.id
    }')" ><i class="fa fa-trash"></i></div>
  </div>`;
    listCart += cartIteam;
  });
  return listCart;
};
// Biến giỏ hàng thành ô input khi mua hàng
let inputCart = (id) => {
  domSelec(
    `#ListInputAddSubtract${id}`
  ).innerHTML = `<input min="0" onchange="addSubtractInput('${id}')" id="inputAddSubtract${id}" type="number" value="1">`;
};
