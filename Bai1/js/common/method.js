// in danh sách sản phẩm ra màn hình bằng API
let inSanPhamRaManHinh = (list) => {
  let listSanPham = ``;
  list.forEach((sp) => {
    let MotsanPham = `${sanPham(sp)}`;
    listSanPham += MotsanPham;
  });
  domSelec("#danhSachSanPham").innerHTML = listSanPham;
};
// in danh sách sản phẩm đã mua ra giỏ hàng
let inSanPhamRaGioHang = (arr) => {
  let listCartItem = ListcartItem(arr);
  domSelec("#cartListItem").innerHTML = listCartItem;
};
//  Loại sản phẩm : samsung,iphone
let loaiSanPham = (sp) => {
  let typeSanPham = domSelec("#loaiSanPham").value;
  let danhSachLoaiSanPham;
  if (typeSanPham == 0) {
    danhSachLoaiSanPham = sp;
  } else {
    danhSachLoaiSanPham = sp.filter((sanpham) => sanpham.type == typeSanPham);
  }
  return danhSachLoaiSanPham;
};
// tìm vị trí sản phẩm trong danh sách sản phẩm
let indexProduct = (arr, id) => {
  let index = arr.findIndex((vitri) => vitri.id == id);
  return index;
};
// tìm vị trí sản phẩm trong giỏ hàng
let indexProDuctCart = (arr, id) => {
  let index = arr.findIndex((vitri) => vitri.product.id == id);
  return index;
};
// mở và tắt giỏ hàng
let openCartBox = () => {
  domSelec("#cart_content").style.width = "60vw";
};

let closeCartBox = () => {
  domSelec("#cart_content").style.width = "0vw";
};
// Đếm số lượng sản phẩm trong giỏ hàng
let demSoLuong = (arr) => {
  let soLuong = 0;
  arr.forEach((item) => {
    soLuong += item.amount * 1;
  });
  domSelec("#cartNum").innerHTML = soLuong;
};
//Remove sản phẩm
let removeIconSp = (arrItemCart, index, id) => {
  if (arrItemCart[index].amount <= 0) {
    arrItemCart.splice(index, 1);
    domIconAddCart(id);
  }
};
// Tính tổng tiền
let tongTien = (arr) => {
  let sum = 0;
  arr.forEach((item) => {
    let sumItem = item.amount * item.product.price;
    sum += sumItem;
  });
  domSelec("#tongTien").innerHTML = sum.toLocaleString();
  domSelec("#tongTienThanhToan").innerHTML = `${sum.toLocaleString()} $ `;
};
// Bật loading
let turnOnLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
let turnOffLoading = () => {
  document.getElementById("loading").style.display = "none";
};
