let renderTable = (listSp) => {
  let contentHTML = ``;
  listSp.forEach((item) => {
    let trContent = `
        <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td><img src=${item.img} style="width:80px"</td>
    
        <td>${item.desc}</td>
        <td>
        <button onclick="suaSanPham(${item.id})" class="btn btn-info"><i class="fa fa-edit" data-toggle="modal" data-target="#myModal"></i></button>
        <button onclick="xoaSanPham(${item.id})" class="btn btn-danger"><i class="fa fa-trash"></i></button>
        </td>
        </tr>`;
    contentHTML += trContent;
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
  });
};

let layThongTinTuForm = () => {
  let id = document.getElementById("txt-stt").value;
  let name = document.getElementById("txt-name").value.trim();
  let price = document.getElementById("price").value.trim();
  let screen = document.getElementById("screen").value.trim();
  let backCamera = document.getElementById("backCamera").value.trim();
  let frontCamera = document.getElementById("frontCamera").value.trim();
  let img = document.getElementById("img").value.trim();
  let desc = document.getElementById("moTa").value.trim();
  let type = document.getElementById("typePhone").value;
  return {
    id: id,
    name: name,
    price: price,
    screen: screen,
    backCamera: backCamera,
    frontCamera: frontCamera,
    img: img,
    desc: desc,
    type: type,
  };
};

let hienThiThongTin = (phone) => {
  document.getElementById("txt-stt").value = phone.id;
  document.getElementById("txt-name").value = phone.name;
  document.getElementById("price").value = phone.price;
  document.getElementById("screen").value = phone.screen;
  document.getElementById("backCamera").value = phone.backCamera;
  document.getElementById("frontCamera").value = phone.frontCamera;
  document.getElementById("img").value = phone.img;
  document.getElementById("moTa").value = phone.desc;
  document.getElementById("typePhone").value = phone.type;
};
// Bật loading
let turnOnLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
let turnOffLoading = () => {
  document.getElementById("loading").style.display = "none";
};
