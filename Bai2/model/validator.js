var validator = {
  kiemTraChuoiRong: function (valueInput, idError) {
    if (valueInput.trim() == "") {
      document.getElementById(idError).innerText = "Bạn chưa nhập thông tin";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = "none";
      return true;
    }
  },

  kiemTraChuoiSo: function (valueInput, idError) {
    var regex = /^[0-9]+$/;

    if (regex.test(valueInput)) {
      console.log("yes");
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = "none";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Trường này chỉ được nhập số";
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },

  kiemTraLoai: function (valueInput, idError) {
    if (valueInput == "Iphone" || valueInput == "Samsung") {
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = "none";
      return true;
    } else {
      document.getElementById(idError).innerText = "Chưa chọn loại điện thoại";
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },
};
