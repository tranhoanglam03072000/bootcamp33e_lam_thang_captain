let dssp = [];
const BASE_URL = "https://62f8b754e0564480352bf3de.mockapi.io";

//LẤY THÔNG TIN RA BẢNG
let renderDssp = () => {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/phone`,
    method: "GET",
  })
    .then((res) => {
      dssp = res.data;
      renderTable(dssp);
      turnOffLoading();
    })
    .catch((err) => {
      turnOffLoading();
      console.log("Lỗi");
    });
};
renderDssp();

//THÊM SP
let themSanPham = () => {
  turnOnLoading();

  var dataForm = layThongTinTuForm();
  let sp = new sanPham(
    dataForm.id,
    dataForm.name,
    dataForm.price,
    dataForm.screen,
    dataForm.backCamera,
    dataForm.frontCamera,
    dataForm.img,
    dataForm.desc,
    dataForm.type
  );
  //name
  let isValid = validator.kiemTraChuoiRong(dataForm.name, "tbTenSp");
  //price
  isValid =
    isValid & validator.kiemTraChuoiRong(dataForm.price, "tbPrice") &&
    validator.kiemTraChuoiSo(dataForm.price, "tbPrice");
  //screen
  isValid = isValid & validator.kiemTraChuoiRong(dataForm.screen, "tbScreen");

  //backcamera
  isValid =
    isValid & validator.kiemTraChuoiRong(dataForm.backCamera, "tbBackCamera");

  //frontcamera
  isValid =
    isValid & validator.kiemTraChuoiRong(dataForm.frontCamera, "tbFrontCamera");

  //img
  isValid = isValid & validator.kiemTraChuoiRong(dataForm.img, "tbImg");

  //desc
  isValid = isValid & validator.kiemTraChuoiRong(dataForm.desc, "tbMota");
  //type
  isValid = isValid & validator.kiemTraLoai(dataForm.type, "tbTypePhone");

  if (isValid == false) {
    return;
  }
  axios({
    url: `${BASE_URL}/phone`,
    method: "POST",
    data: sp,
  })
    .then((res) => {
      renderTable();

      $("#myModal").modal("hide");
      alert("Bạn đã update thành công sản phẩm");
      location.reload();
      turnOffLoading();
    })
    .catch((err) => {
      turnOffLoading();
      console.log("Lỗi");
    });
};

// XÓA SP
let xoaSanPham = (id) => {
  turnOnLoading();

  $("#delete-modal").modal("show");
  axios({
    url: `${BASE_URL}/phone/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      renderDssp();

      location.reload();
      turnOffLoading();
    })
    .catch((err) => {
      turnOffLoading();
      console.log("Lỗi");
    });
};

// SỬA SP
let suaSanPham = (id) => {
  turnOnLoading();

  axios({
    url: `${BASE_URL}/phone/${id}`,
    method: "GET",
  })
    .then((res) => {
      hienThiThongTin(res.data);
      turnOffLoading();
    })
    .catch((err) => {
      turnOffLoading();
      console.log("Lỗi");
    });
};

//UPDATE SP
let capNhatSanPham = () => {
  turnOnLoading();

  let dataSp = layThongTinTuForm();

  //name
  let isValid = validator.kiemTraChuoiRong(dataSp.name, "tbTenSp");
  //price
  isValid =
    isValid & validator.kiemTraChuoiRong(dataSp.price, "tbPrice") &&
    validator.kiemTraChuoiSo(dataSp.price, "tbPrice");
  //screen
  isValid = isValid & validator.kiemTraChuoiRong(dataSp.screen, "tbScreen");

  //backcamera
  isValid =
    isValid & validator.kiemTraChuoiRong(dataSp.backCamera, "tbBackCamera");

  //frontcamera
  isValid =
    isValid & validator.kiemTraChuoiRong(dataSp.frontCamera, "tbFrontCamera");

  //img
  isValid = isValid & validator.kiemTraChuoiRong(dataSp.img, "tbImg");

  //desc
  isValid = isValid & validator.kiemTraChuoiRong(dataSp.desc, "tbMota");
  //type
  isValid = isValid & validator.kiemTraLoai(dataSp.type, "tbTypePhone");

  if (isValid == false) {
    return;
  }

  axios({
    url: `${BASE_URL}/phone/${dataSp.id}`,
    method: "PUT",
    data: dataSp,
  })
    .then((res) => {
      renderDssp();
      document.getElementById("myform").reset();
      location.reload();
      turnOffLoading();
    })
    .catch((err) => {
      turnOffLoading();
      console.log("Lỗi");
    });
};

document.getElementById("btnThem").onclick = () => {
  document.getElementById("myform").reset();
};
